
var studentsAndPoints = ['Алексей Петров', 0, 'Ирина Овчинникова', 60, 'Глеб Стукалов', 30, 'Антон Павлович', 30, 'Виктория Заровская', 30, 'Алексей Левенец', 70, 'Тимур Вамуш', 30, 'Евгений Прочан', 60, 'Александр Малов', 0];

//доработка: Вывод первоначального списка студентов (к заданию №1), вывод максимального балла (к заданию №2)
	console.log( 'Список студентов:' );
	var RatingIndex;
	var maxRating = -1;
//дополнено: шаг итерации - 2
	for (var s = 0; s < studentsAndPoints.length; s+=2) {
		console.log( 'Студент ' + studentsAndPoints[s] + ' набрал ' + studentsAndPoints[s+1] + ' баллов' );
		if (maxRating < 0 || studentsAndPoints[s+1] > maxRating) {
				maxRating = studentsAndPoints[s+1];
				RatingIndex = s;
		}
	}
	console.log( 'Студент набравший максимальный балл:' );
	console.log( 'Студент ' + studentsAndPoints[RatingIndex] + ' имеет максимальный балл ' + maxRating );

//Добавление новых студентов
studentsAndPoints.push('Николай Фролов', 0, 'Олег Боровой', 0);


//Внесение изменений значений баллов
//дополнение: провести проверку на наличие студентов в списках
	//var applyStudents = ['Антон Павлович', 'Николай Фролов', 'Дмитрий Фитискин', 'Василий Теркин', 'Иван Тараканов'];
	var applyStudents = ['Алексей Петров', 'Николай Фролов', 'Дмитрий Фитискин', 'Василий Теркин', 'Иван Тараканов'];
	for (var t = 0; t < applyStudents.length; t++ ) {
		if ( studentsAndPoints.indexOf(applyStudents[t]) !== -1 ) {
			studentsAndPoints [ studentsAndPoints.indexOf( applyStudents[t] ) + 1 ] += 10;
			console.log(applyStudents[t]);
		}
	}


//Получение списка студентов с нулевым баллом. Вывод списка в консоль
	console.log('Студенты не набравшие баллов:');
	var dismiss = new Array(); //Массив ключей для удаления
	for (s = 0; s < studentsAndPoints.length; s++) {
		if (studentsAndPoints[s] == 0) {
			IndexForDelete = s-1;
			dismiss.push(IndexForDelete);
			console.log( studentsAndPoints[ IndexForDelete ]);
		}
		}
		
//Исключение студентов с нулевым баллом
	dismiss.reverse();
	for (i = 0; i < dismiss.length; i++) {
		console.log( 'Исключен из списка: ' + studentsAndPoints[ dismiss[i] ]);
		studentsAndPoints.splice(dismiss[i], 2);
	}
		
//Оставшиеся в группе
	console.log('Студенты оставшиеся в группе:');
	for (var s = 0; s < studentsAndPoints.length; s++) {
		if (s % 2 == 0) {
			student = studentsAndPoints[s];
		}
		else {
			console.log( student + ' — ' + studentsAndPoints[s] + ' баллов' );
		}
	}